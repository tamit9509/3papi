# Documentation

## To start server run following commands

1. Inside the project directory, run: npm Install
2. npm start or node server.js
   You can see swagger documentation on http://localhost:4000/documentation

## Steps To run the functionality

1. API: /news

   - To access this API you need to /api/register first and get access token using /api/login API
   - Filters in API:
     1. **q**: parameter used to search news
     2. **category**: The category you want to get headlines for
     3. **pageSize**: The number of results to return per page (request). 20 is the default, 100 is the maximum
     4. **page**: Use this to page through the results if the total results found is greater than the page size.
     5. **authorization**: Place your accessToken in this parameter

2. API /api/weather:
   - No accessToken required for this API. You need to add lat, lon of the place. This API will give you update about the weather of that place.
   - Get lat lon from **/api/locations** to save time.
