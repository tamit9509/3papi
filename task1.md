# Create a function to read 10 gigabytes of data and sort it.

- 1 GB RAM 10 GB unsorted Array in a file

## Algo

1. Read 100 mb data from file;
2. const sortedData = mergeSort(10_mb_data_array );
3. write sortedData to output Buffer of 100 mb
4. Loop step 1,2,3 and store sorted data in output buffer
5. Whenever the output buffer fills, write it to the final sorted file and empty it.
