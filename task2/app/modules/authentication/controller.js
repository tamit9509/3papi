const { MONGO_ERROR } = require('../../utils/constants');
const util = require('../../utils/utils');
const { UserModel, SessionModel } = require('../../mongo-models');
const commonFunctions = require('../../utils/commonFunctions');
const errorCodes = require('../../utils/errorCodes');

const controller = {
  userRegister: async (request, response) => {
    request.body.password = commonFunctions.hashPassword(request.body.password);
    request.body.email = request.body.email.toLowerCase();
    const user = new UserModel(request.body);
    try {
      const data = await user.save();
      response.status(200).json({
        success: true,
        message: 'User registered successfully',
        data,
      });
    } catch (err) {
      if (err.code == MONGO_ERROR.DUPLICATE) {
        throw errorCodes.ACCOUNT_ALREADY_EXIST;
      }
    }
  },
  userLogin: async (request, response) => {
    const user = await UserModel.findOne({
      email: request.body.email.toLowerCase(),
    }).lean();
    if (
      !user ||
      !commonFunctions.compareHash(request.body.password, user.password)
    ) {
      throw errorCodes.INVALID_CREDENTIALS;
      // response.status(400).json({
      //   success: false,
      //   message: 'Invalid credentials',
      // });
    } else {
      const tokenPayload = {
        role: user.role,
        id: user._id,
      };
      const accessToken = commonFunctions.encryptJwt(tokenPayload);
      const sessionPayload = {
        userId: user._id,
        accessToken,
        deviceToken: request.body.deviceToken,
        role: user.role,
      };
      let session;
      if (request.body.deviceToken) {
        session = await SessionModel.findOneAndUpdate(
          { deviceToken: request.body.deviceToken },
          sessionPayload,
          { upsert: true, new: true }
        ).lean();
      } else {
        session = await new SessionModel(sessionPayload).save();
      }
      response.status(200).json({
        success: true,
        message: 'Login successfull',
        data: {
          accessToken: session.accessToken,
          name: user.name,
        },
      });
    }
  },
  logoutSession: async (request, response) => {
    await SessionModel.deleteOne({ _id: request.user._id });
    response.status(200).json({
      success: true,
      message: 'Logout successfully',
    });
  },
};

module.exports = { authController: controller };
