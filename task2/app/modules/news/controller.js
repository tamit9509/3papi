const News = require('./service');

const controller = {
  async getNews(request, response) {
    const { category, q, pageSize, page } = request.query;
    const news = new News();
    try {
      await news.getNewsHeadLine(category, q, pageSize, page);
      response.status(200).json({
        success: true,
        data: news,
      });
    } catch (err) {
      response.status(400).json({
        success: false,
        data: err.message,
      });
    }
  },
};
module.exports = { newsController: controller };
