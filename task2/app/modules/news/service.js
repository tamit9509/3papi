const httpRequest = require('request');
const _ = require('lodash');
const moment = require('moment');
const params = require('../../../config/env/development_params.json');
const { EXTERNAL_APIS } = require('../../utils/constants');

module.exports = class News {
  constructor() {}

  async getNewsHeadLine(category, q, pageSize, page) {
    return new Promise((resolve, reject) => {
      const url = `${params.NewsApi}${EXTERNAL_APIS.NEWS}?apiKey=${params.NewsAPIId}&category=${category}&pageSize=${pageSize}&page=${page}&q=${q}`;
      console.log(url);
      httpRequest(
        {
          method: 'GET',
          uri: url,
        },
        (err, res, b) => {
          if (err) {
            reject(err);
            return;
          }
          try {
            this.filterResponseData(JSON.parse(b));
          } catch (err) {
            reject(err);
          }
          resolve(true);
        }
      );
    });
  }
  /**
   * Function set weather data for upcoming days
   * @param {*} obj response from API of onecall
   */
  filterResponseData(obj) {
    if (obj.status == 'error') {
      throw obj;
    }
    this.data = obj.articles.map((obj) => {
      return {
        headline: obj.title,
        description: obj.description,
        url: obj.url,
        publishedAt: obj.publishedAt,
        content: obj.content,
      };
    });
    this.counts = this.data.length;
  }
};
