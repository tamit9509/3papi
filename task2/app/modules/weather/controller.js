const locations = require('../../../config/env/location.json');
const Weather = require('./service');
const controller = {
  async weatherForeCast(request, response) {
    const { lat, lon } = request.query;
    const { unit } = request.query;
    const weather = new Weather();
    try {
      await weather.getWeatherUpdate(lat, lon, unit);
      response.status(200).json({
        success: true,
        data: weather,
      });
    } catch (err) {
      response.status(400).json({
        success: false,
        data: err.message,
      });
    }
  },
  getLocations(request, response) {
    response.status(200).json({
      success: true,
      data: locations,
    });
  },
};
module.exports = { weatherController: controller };
