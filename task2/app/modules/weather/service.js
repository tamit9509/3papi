const httpRequest = require('request');
const moment = require('moment');
const params = require('../../../config/env/development_params.json');
const location = require('../../../config/env/location.json');
const { EXTERNAL_APIS, WEATHER_PARAMS } = require('../../utils/constants');
module.exports = class Weather {
  constructor() {}
  /**
   *
   * @param {*} lat Latitude of location
   * @param {*} lon Logitude of location
   * @returns Next 7 days weather update
   */
  async getWeatherUpdate(lat, lon, unit) {
    this.unit = unit;
    const obj = location.find((city) => city.lat == lat && city.lon == lon);
    this.location = obj ? obj.city : 'Unknown';
    return new Promise((resolve, reject) => {
      const url = `${params.OpenWeather}${EXTERNAL_APIS.WEATHER_UPDATE}?appid=${params.WeatherAPIId}&lat=${lat}&lon=${lon}&exclude=${WEATHER_PARAMS.ONE_CALL_EXCLUDE}&unit=${unit}`;
      httpRequest(
        {
          method: 'GET',
          uri: url,
        },
        (err, res, b) => {
          if (err) {
            reject(err);
            return;
          }
          try {
            this.filterResponseData(JSON.parse(b));
          } catch (err) {
            reject(err);
          }
          resolve(true);
        }
      );
    });
  }
  /**
   * Function set weather data for upcoming days
   * @param {*} obj response from API of onecall
   */
  filterResponseData(obj) {
    if (obj.cod == 400) {
      throw obj;
    }
    this.data = obj.daily.map((stats) => {
      return {
        date: moment(stats.dt * 1000).format('ddd, MMMM DD YYYY'),
        main: stats.weather[0].main,
        description: stats.weather[0].description,
        temp: stats.temp.day,
      };
    });
    this.counts = this.data.length;
  }
};
