const MONGOOSE = require('mongoose');
const { USER_ROLE, web_view } = require('../utils/constants');

const Schema = MONGOOSE.Schema;

const schema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'users' },
  accessToken: { type: String, required: true },
});

schema.set('timestamps', true);
schema.index({ accessToken: 1 }, { unique: true });
const SessionModel = MONGOOSE.model('session', schema);
module.exports = { SessionModel };
