const MONGOOSE = require('mongoose');
const { USER_ROLE } = require('../utils/constants');
const schema = MONGOOSE.Schema;

const user = new schema({
  name: { type: String, require: true, trim: true },
  email: { type: String, require: true, unique: true, trim: true },
  password: { type: String, require: true },
});

user.set('timestamps', true);
const UserModel = MONGOOSE.model('user', user);
module.exports = { UserModel };
