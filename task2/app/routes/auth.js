const JOI = require('joi');
const { authController } = require('../modules/authentication/controller');

const routes = [
  {
    path: '/api/register',
    method: 'POST',
    joiSchemaForSwagger: {
      body: JOI.object({
        name: JOI.string().required().description('User name'),
        email: JOI.string()
          .email()
          .required()
          .description('User email for registration'),
        password: JOI.string().description('Password'),
      }),
      group: 'User Authentication',
      description: 'Api to register user',
      model: 'RegisterUser',
    },
    handler: authController.userRegister,
  },
  {
    path: '/api/login',
    method: 'POST',
    joiSchemaForSwagger: {
      body: {
        email: JOI.string().email().required(),
        password: JOI.string().required(),
      },
      group: 'User Authentication',
      description: 'Api to login',
      model: 'UserLogin',
    },
    handler: authController.userLogin,
  },
  {
    path: '/api/logout',
    method: 'GET',
    joiSchemaForSwagger: {
      headers: JOI.object({
        authorization: JOI.string().required(),
      }).unknown(),
      group: 'User Authentication',
      description: 'Api to logout password',
      model: 'logout',
    },
    auth: true,
    handler: authController.logoutSession,
  },
];

module.exports = routes;
