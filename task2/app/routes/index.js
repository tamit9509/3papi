module.exports = [
  ...require('./auth'),
  ...require('./news'),
  ...require('./weather'),
];
