const joi = require('joi');

const { newsController } = require('../modules/news/controller');
const { NEWS_PARAMS } = require('../utils/constants');

const routes = [
  {
    path: '/news',
    method: 'GET',
    joiSchemaForSwagger: {
      headers: joi
        .object({
          authorization: joi.string().required(),
        })
        .unknown(),
      query: {
        q: joi
          .string()
          .default('')
          .description('Keywords or a phrase to search for.'),
        category: joi
          .string()
          .valid(Object.values(NEWS_PARAMS.CATEGORY))
          .description('The category you want to get headlines for.'),
        pageSize: joi
          .number()
          .default(NEWS_PARAMS.PAGE_SIZE)
          .description(
            'The number of results to return per page (request). 20 is the default, 100 is the maximum.'
          ),
        page: joi
          .number()
          .description(
            'Use this to page through the results if the total results found is greater than the page size.'
          ),
      },
      group: 'News',
      description: 'API to fetch top headlines',
      model: 'FetchNews',
    },
    auth: true,
    handler: newsController.getNews,
  },
];
module.exports = routes;
