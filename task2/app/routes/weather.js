const joi = require('joi');

const { weatherController } = require('../modules/weather/controller');
const { WEATHER_PARAMS } = require('../utils/constants');

const routes = [
  {
    path: '/api/locations',
    method: 'GET',
    joiSchemaForSwagger: {
      group: 'Weather',
      description: 'API to get Locations weather update',
      model: 'GetLocations',
    },
    handler: weatherController.getLocations,
  },
  {
    path: '/api/weather',
    method: 'GET',
    joiSchemaForSwagger: {
      query: {
        lat: joi.number().required(),
        lon: joi.number().required(),
        unit: joi
          .string()
          .valid(Object.values(WEATHER_PARAMS.UNITS))
          .default(WEATHER_PARAMS.UNITS.METRIC),
      },
      group: 'Weather',
      description: 'API to get weather update',
      model: 'GetWeatherUpdate',
    },
    handler: weatherController.weatherForeCast,
  },
];
module.exports = routes;
