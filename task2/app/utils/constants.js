const CONSTANTS = {
  SECURITY: {
    JWT_SIGN_KEY: 'fasdkfjklandfkdsfjladsfodfafjalfadsfkads',
    BCRYPT_SALT: 8,
  },
  EXTERNAL_APIS: {
    WEATHER_UPDATE: '/data/2.5/onecall',
    NEWS: '/v2/top-headlines',
  },
  WEATHER_PARAMS: {
    ONE_CALL_EXCLUDE: 'current,minutely,hourly',
    UNITS: {
      STANDARD: 'standard',
      METRIC: 'metric',
      IMPERIAL: 'imperial',
    },
  },
  NEWS_PARAMS: {
    CATEGORY: {
      TECHNOLOGY: 'technology',
      SPORTS: 'sports',
      SCIENCE: 'science',
      HEALTH: 'health',
      GENERAL: 'general',
      ENTERTAINMENT: 'entertainment',
      BUSINESS: 'business',
    },
    PAGE_SIZE: 20,
  },
};
module.exports = CONSTANTS;
