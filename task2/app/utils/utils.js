'use strict';

const BCRYPT = require('bcrypt');
const fs = require('fs');
const CONFIG = require('../../config/config');
// const transporter = require('nodemailer').createTransport(CONFIG.Development.NODE_MAILER.transporter);
const JWT = require('jsonwebtoken');
const handleBar = require('handlebars');
// const {aws}=require('../services/aws');
const path = require('path');

//apiKeys for Mailjet
const CONSTANTS = require('../utils/constants');

const utils = {};

/**
 * incrypt password in case user login implementation
 * @param {*} payloadString
 */
utils.hashPassword = (payloadString) => {
  return BCRYPT.hashSync(payloadString, CONSTANTS.SECURITY.BCRYPT_SALT);
};

/**
 * @param {string} plainText
 * @param {string} hash
 */
utils.compareHash = (payloadPassword, userPassword) => {
  return BCRYPT.compareSync(payloadPassword, userPassword);
};

/**
 * Send emaildetails object in params
 * @param {*} email
 */

/** create jsonwebtoken **/
utils.encryptJwt = (payload) => {
  const token = JWT.sign(payload, CONSTANTS.SECURITY.JWT_SIGN_KEY, {
    algorithm: 'HS256',
  });
  return token;
};

utils.decryptJwt = (token) => {
  return JWT.verify(token, CONSTANTS.SECURITY.JWT_SIGN_KEY, {
    algorithm: 'HS256',
  });
};

/**
 * function to create random string.
 */
// utils.createRandomString = (length = 12) => {
//   var result = '';
//   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//   var charactersLength = characters.length;
//   for (var i = 0; i < length; i++) {
//     result += characters.charAt(Math.floor(Math.random() * charactersLength));
//   }
//   return result;
// };

module.exports = utils;
