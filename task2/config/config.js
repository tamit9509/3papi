'use strict';

const nconf = require('nconf');

// variable to import to nconf from process.env
const envVariables = [
  'PORT',
  'NODE_ENV',
  'PRIVATE_SALT',
  'PRIVATE_SALT',
  'PRIVATE_AUTH_TOKEN',
  'PRIVATE_API_KEY',
  'TEST_PRIVATE_SALT',
  'TEST_PRIVATE_AUTH_TOKEN',
  'TEST_PRIVATE_API_KEY',
];

//
// Setup nconf to use (in-order):
//   1. Locally computed config
//   2. Command-line arguments
//   3. Some Environment variables
//   4. Some defaults
//   5. Environment specific config file located at './env/<NODE_ENV>.json'
//   6. Shared config file located at './env/all.json'
//
nconf
  .argv()
  .env(envVariables) // Load select environment variables
  .defaults({
    store: {
      NODE_ENV: 'development',
    },
  });

module.exports = nconf.get();
