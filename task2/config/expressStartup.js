const express = require('express');
const app = express();
const cors = require('cors');
const utils = require('../app/utils/routeUtils');
const routes = require('../app/routes');
const logger = require('../config/winston');

const expressStartup = async () => {
  // app.use(bodyParser.json({ limit: '50mb' }));
  // app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

  /********************************
   ***** Server Configuration *****
   ********************************/
  app.set('port', 4000);
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ limit: '50mb', extended: false }));

  /********************************
   ***** For handling CORS Error ***
   *********************************/
  app.use(cors());
  app.all('/*', (request, response, next) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.header(
      'Access-Control-Allow-Headers',
      'Content-Type, api_key, Authorization, x-requested-with, Total-Count, Total-Pages, Error-Message'
    );
    response.header(
      'Access-Control-Allow-Methods',
      'POST, GET, DELETE, PUT, OPTION'
    );
    response.header('Access-Control-Allow-Credentials', 'true');
    response.header('Access-Control-Max-Age', 1800);
    next();
  });

  process.on('uncaughtException', (reason) => {
    console.log('uncaughtException', reason);
    process.exit(1);
  });
  process.on('unhandledRejection', (reason) => {
    console.log('unhandledRejection', reason);
    process.exit(1);
  });

  await utils.initRoutes(app, routes);
  app.listen(process.env.PORT || 4000, '0.0.0.0', () => {
    logger.info('server is start at port ' + process.env.PORT || 4000);
    logger.info(
      `Swagger Documentation URL: http://localhost:${
        process.env.PORT || 4000
      }/documentation`
    );
  });
};
module.exports = expressStartup;
