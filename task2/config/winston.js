'use strict';

const winston = require('winston');
const { combine, simple, colorize } = winston.format;
// const errorStackFormat = format(info => {
//     if (info instanceof Error) {
//         return Object.assign({}, info, {
//             stack: info.stack,
//             message: info.message
//         })
//     }
//     return info;
// })
const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json(),
    winston.format.prettyPrint()
  ),
  transports: [new winston.transports.Console()],
});

module.exports = logger;
